import { Router } from 'express';

import { mountAppRoutes } from './routes';

/**
 * Mounts all routes for the app
 *
 * @export
 */
export function mountRoutes(router: Router) {
    const appRoute = process.env.TODO_ROUTE || '/api/todo';
    mountAppRoutes(router, appRoute);
}
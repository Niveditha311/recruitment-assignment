import { Router } from 'express';

import { restaurant } from '../controllers/restaurant';

export function mountAppRoutes(router: Router, route: string) {
    const controller = new restaurant();

    router.get(route, (req, res) => {
        controller.get(req, res)
        .then(() => {
            // do nothing
        })
        .catch(error => {
            // log error
        });
    });

    router.post(route, (req, res) => {
        controller.post(req, res)
        .then(() => {
            // do nothing
        })
        .catch(error => {
            // log error
        });
    });

    router.put(route, (req, res) => {
        controller.put(req, res);
    });

    
}
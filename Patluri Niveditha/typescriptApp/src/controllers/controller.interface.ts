import { Request, Response } from 'express';

/**
 * interface defining the CRUD methods for a controller
 *
 * @export
 */
export interface IController {
    login(req: Request, res: Response);
    fooditems(req: Request, res: Response);
    register(req: Request, res: Response);
    checkout(req: Request, res: Response);
}
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomersComponent } from './customers/customers.component';
import { FooditemsComponent } from './fooditems/fooditems.component';
import { HistorydetailsComponent } from './historydetails/historydetails.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomersComponent,
    FooditemsComponent,
    HistorydetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class CustomerDetails {
    id: number;
    name: string;
    email : string;
    phone : string;
  }
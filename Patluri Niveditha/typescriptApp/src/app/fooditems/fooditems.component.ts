import { Component, OnInit } from '@angular/core';
import { MongodbDataService } from '../mongodb-data.service'
import {Food} from '../food'
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'

@Component({
  selector: 'app-fooditems',
  templateUrl: './fooditems.component.html',
  styleUrls: ['./fooditems.component.css']
})


export class FooditemsComponent implements OnInit {

  foodDetails: Food = null;

  constructor(private activeRoute: ActivatedRoute, private router : Router) { }

  ngOnInit() {
    this.getCustomerDetails();
  }


  getCustomerDetails(): void {
    this.activeRoute.params.subscribe(routeParams => {

      this.MongodbDataService.getCustomerDetails(routeParams.id).subscribe(

        (foodDetails: CustomerDetails) => {
          this.foodDetails = foodDetails;
          console.log(foodDetails);
        });

    });

  }
}

import { Component, OnInit } from '@angular/core';
import { CustomerDetails } from '../customerdetails'
import { MongodbDataService } from '../mongodb-data.service'
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'
@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customerDetails: CustomerDetails = null;

  constructor(private activeRoute: ActivatedRoute, private router : Router) { }

  ngOnInit() {
    this.getCustomerDetails();
  }


  getCustomerDetails(): void {
    this.activeRoute.params.subscribe(routeParams => {

      this.MongodbDataService.getCustomerDetails(routeParams.id).subscribe(

        (customerDetails: CustomerDetails) => {
          this.customerDetails = customerDetails;
          console.log(customerDetails);
        });

    });

  }


}
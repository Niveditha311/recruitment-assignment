import { TestBed } from '@angular/core/testing';

import { MongodbDataService } from './mongodb-data.service';

describe('MongodbDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MongodbDataService = TestBed.get(MongodbDataService);
    expect(service).toBeTruthy();
  });
});
